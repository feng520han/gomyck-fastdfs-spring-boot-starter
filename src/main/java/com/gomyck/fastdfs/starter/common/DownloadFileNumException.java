package com.gomyck.fastdfs.starter.common;

/**
 * @author gomyck
 * @version 1.0.0
 */
public class DownloadFileNumException extends RuntimeException {

    public DownloadFileNumException(String message) {
        super(message);
    }

}
